package twodb;

import javax.sql.DataSource;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;

@SpringBootApplication
public class TwoDatabasesDemoApplication {
	public static void main(String[] args) {
		SpringApplication.run(TwoDatabasesDemoApplication.class, args);
		System.out.println("EXIT!");
	}
}
