package twodb;

import java.util.HashMap;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@PropertySource({ "classpath:config_medium.properties" })
@EnableTransactionManagement
@EnableJpaRepositories(entityManagerFactoryRef = "secondEntityManager",
		transactionManagerRef = "secondTransactionManager", 
		basePackages = { "twodb.second.repo" })
public class ConfigMediumDB2 {
	
    @Autowired
    private Environment env;
	
	@Bean
	@ConfigurationProperties(prefix = "testdb2.datasource")
	public DataSource secondDataSource() {
		return DataSourceBuilder.create().build();
	}

	@Bean
	public LocalContainerEntityManagerFactoryBean secondEntityManager(
			EntityManagerFactoryBuilder builder,
			@Qualifier("secondDataSource") DataSource dataSource) {
		
		HashMap<String, Object> properties = new HashMap<>();
		properties.put("hibernate.hbm2ddl.auto",
				env.getProperty("spring.jpa.hibernate.ddl-auto"));
		return builder.dataSource(dataSource).
				packages("twodb.second").
				properties(properties).
				persistenceUnit("second").build();
	}
	
	@Bean(name = "secondTransactionManager")
	public PlatformTransactionManager secondTransactionManager(
			@Qualifier("secondEntityManager") EntityManagerFactory entityManager) {
		return new JpaTransactionManager(entityManager);
	}
}
